#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringListModel>
#include <QPushButton>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void bt_0(){
        processDigit('0');
    }
    void bt_1(){
        processDigit('1');
    }
    void bt_2(){
        processDigit('2');
    }
    void bt_3(){
        processDigit('3');
    }
    void bt_4(){
        processDigit('4');
    }
    void bt_5(){
        processDigit('5');
    }
    void bt_6(){
        processDigit('6');
    }
    void bt_7(){
        processDigit('7');
    }
    void bt_8(){
        processDigit('8');
    }
    void bt_9(){
        processDigit('9');
    }
    void dot(){
        processDigit('.');
        btDot->setEnabled(false);
    }

    void plus();

    void minus();

    void multiply();

    void divide();

    void eq();

    void histUp();

    void histDown();

    void undo();

    void clear();

    void clearAll();

private:
    void printOp(char op);

    void checkSwitchRow(QString row);

    double eval(QString &str);

    int histOffset = 0;
    QString curRow;

    enum InputState{START, LEFT_OP, RIGHT_OP, EQ};

    InputState inputState = START;

    Ui::MainWindow *ui;

    QStringList *history;

    QStringListModel *listModel;

    QPushButton *btEq;
    QPushButton *btPlus;
    QPushButton *btMinus;
    QPushButton *btMult;
    QPushButton *btDiv;
    QPushButton *btDot;

    void updateUI();

    void updateButtons();

    void processDigit(char dig);
};
#endif MAINWINDOW_H
