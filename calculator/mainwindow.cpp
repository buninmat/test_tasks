#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    btEq = ui->pushButton_eq;
    btPlus = ui->pushButton_plus;
    btMinus = ui->pushButton_minus;
    btDiv = ui->pushButton_div;
    btMult = ui->pushButton_mult;
    btEq->setEnabled(false);
    btPlus->setEnabled(true);
    btMult->setEnabled(false);
    btDiv->setEnabled(false);
    btMinus->setEnabled(true);

    btEq->setShortcut(QKeySequence(Qt::Key_Enter));
    btMult->setShortcut(QKeySequence(Qt::Key_Asterisk));
    btDiv->setShortcut(QKeySequence(Qt::Key_Slash));
    btPlus->setShortcut(QKeySequence(Qt::Key_Plus));
    btMinus->setShortcut(QKeySequence(Qt::Key_Minus));
    ui->pushButton_0->setShortcut(QKeySequence(Qt::Key_0));
    ui->pushButton_1->setShortcut(QKeySequence(Qt::Key_1));
    ui->pushButton_2->setShortcut(QKeySequence(Qt::Key_2));
    ui->pushButton_3->setShortcut(QKeySequence(Qt::Key_3));
    ui->pushButton_4->setShortcut(QKeySequence(Qt::Key_4));
    ui->pushButton_5->setShortcut(QKeySequence(Qt::Key_5));
    ui->pushButton_6->setShortcut(QKeySequence(Qt::Key_6));
    ui->pushButton_7->setShortcut(QKeySequence(Qt::Key_7));
    ui->pushButton_8->setShortcut(QKeySequence(Qt::Key_8));
    ui->pushButton_9->setShortcut(QKeySequence(Qt::Key_9));
    ui->pushButton_up->setShortcut(QKeySequence(Qt::Key_Up));
    ui->pushButton_down->setShortcut(QKeySequence(Qt::Key_Down));
    ui->pushButton_bsp->setShortcut(QKeySequence(Qt::Key_Backspace));
    ui->pushButton_dot->setShortcut(QKeySequence(Qt::Key_Comma));

    ui->listView->setSelectionMode(QAbstractItemView::SelectionMode::NoSelection);

    connect(ui->pushButton_0, &QPushButton::clicked, this, &MainWindow::bt_0);
    connect(ui->pushButton_1, &QPushButton::clicked, this, &MainWindow::bt_1);
    connect(ui->pushButton_2, &QPushButton::clicked, this, &MainWindow::bt_2);
    connect(ui->pushButton_3, &QPushButton::clicked, this, &MainWindow::bt_3);
    connect(ui->pushButton_4, &QPushButton::clicked, this, &MainWindow::bt_4);
    connect(ui->pushButton_5, &QPushButton::clicked, this, &MainWindow::bt_5);
    connect(ui->pushButton_6, &QPushButton::clicked, this, &MainWindow::bt_6);
    connect(ui->pushButton_7, &QPushButton::clicked, this, &MainWindow::bt_7);
    connect(ui->pushButton_8, &QPushButton::clicked, this, &MainWindow::bt_8);
    connect(ui->pushButton_9, &QPushButton::clicked, this, &MainWindow::bt_9);
    connect(ui->pushButton_plus, &QPushButton::clicked, this, &MainWindow::plus);
    connect(ui->pushButton_minus, &QPushButton::clicked, this, &MainWindow::minus);
    connect(ui->pushButton_mult, &QPushButton::clicked, this, &MainWindow::multiply);
    connect(ui->pushButton_div, &QPushButton::clicked, this, &MainWindow::divide);
    connect(ui->pushButton_eq, &QPushButton::clicked, this, &MainWindow::eq);
    connect(ui->pushButton_up, &QPushButton::clicked, this, &MainWindow::histUp);
    connect(ui->pushButton_down, &QPushButton::clicked, this, &MainWindow::histDown);
    connect(ui->pushButton_bsp, &QPushButton::clicked, this, &MainWindow::undo);
    connect(ui->pushButton_c, &QPushButton::clicked, this, &MainWindow::clear);
    connect(ui->pushButton_ac, &QPushButton::clicked, this, &MainWindow::clearAll);
    connect(ui->pushButton_dot, &QPushButton::clicked, this, &MainWindow::dot);

    ui->pushButton_eq->setEnabled(false);
    ui->pushButton_eq->setEnabled(false);

    history = new QStringList();
    listModel = new QStringListModel(*history, this);
    ui->listView->setModel(listModel);

    btDot = ui->pushButton_dot;
}

MainWindow::~MainWindow()
{
    delete ui;
    delete history;
    delete listModel;
}

void MainWindow::processDigit(char dig){
    checkSwitchRow("");
    QString& last = history->last();
    last = last + dig;

    updateUI();
    histOffset = 0;
}

void MainWindow::plus(){
    checkSwitchRow(history->last());
    printOp('+');
    btDot->setEnabled(true);
}

void MainWindow::minus(){
    checkSwitchRow(history->last());
    printOp('-');
    btDot->setEnabled(true);
}

void MainWindow::checkSwitchRow(QString row){
    if(inputState == EQ || inputState == START){
        inputState = LEFT_OP;
        history->append(row);
    }else{
        inputState = RIGHT_OP;
    }
}

void MainWindow::multiply(){
    checkSwitchRow(history->last());
    printOp('*');
    btDot->setEnabled(true);
}

void MainWindow::divide(){
    checkSwitchRow(history->last());
    printOp('/');
    btDot->setEnabled(true);
}

void MainWindow::eq(){
    inputState = EQ;
    history->append(QString::number(eval(history->last())));
    updateUI();
    histOffset = 0;
}

void MainWindow::histUp(){
    if(histOffset == 0){
        return;
    }
    histOffset--;
    if(histOffset == 0){
        history->last() = curRow;
    }
    history->last() = history->at(history->size() - 1 - histOffset);
    updateUI();
}

void MainWindow::histDown(){
    if(histOffset == history->size() - 1){
        return;
    }
    if(histOffset == 0){
        curRow = history->last();
    }
    histOffset++;
    history->last() = history->at(history->size() - 1 - histOffset);
    updateUI();
}

void MainWindow::undo(){
    if(inputState == START || inputState == EQ){
        return;
    }
    auto& curRow = history->last();
    curRow = curRow.left(curRow.size() - 1);
    updateUI();
    histOffset = 0;
}

void MainWindow::clear(){
    history->last() = "";
    updateUI();
    histOffset = 0;
}

void MainWindow::clearAll(){
    history->clear();
    updateUI();
    histOffset = 0;
}

void MainWindow::printOp(char op){
    QString& last = history->last();
    last = last + ' ' + op + ' ';
    updateUI();
    histOffset = 0;
}

void MainWindow::updateUI(){
    QStringList list(history->rbegin(), history->rend());
    listModel->setStringList(list);
    updateButtons();
}

void MainWindow::updateButtons(){
    switch(inputState){
        case START:
            btEq->setEnabled(false);
            btPlus->setEnabled(false);
            btMult->setEnabled(false);
            btDiv->setEnabled(false);
            btMinus->setEnabled(true);
        break;
        case EQ:
            btEq->setEnabled(false);
            btPlus->setEnabled(true);
            btMult->setEnabled(true);
            btDiv->setEnabled(true);
            btMinus->setEnabled(true);
        break;
        case LEFT_OP:
            btEq->setEnabled(false);
            btPlus->setEnabled(true);
            btMult->setEnabled(true);
            btDiv->setEnabled(true);
            btMinus->setEnabled(true);
        break;
        default:
            btEq->setEnabled(true);
            btPlus->setEnabled(true);
            btMult->setEnabled(true);
            btDiv->setEnabled(true);
            btMinus->setEnabled(true);
        break;
     }
}

double MainWindow::eval(QString &str){
    QStringList tokens = str.split(" ");
    double opdl = 0, opdr;
    std::function<double(double,double)> op = nullptr;
    std::function<double(double,double)> op_prior = nullptr;
    for(QString &token : tokens){
        std::function<double(double,double)> nop = nullptr;
        bool prior = false;
        if(token == "+"){
            nop = std::plus<double>();
        }else if(token == "-"){
            nop = std::minus<double>();
        }else if(token == "*"){
            nop = std::multiplies<double>();
            prior = true;
        }else if(token == "/"){
            nop = std::divides<double>();
            prior = true;
        }else{ //number
            if(op == nullptr){
                opdl = token.toDouble();
            }else{
                double nopd = token.toDouble();
                if(op_prior == nullptr){
                    opdr = nopd;
                }else{
                    opdr = op_prior(opdr, nopd);
                    op_prior = nullptr;
                }
            }
        }
        if(nop != nullptr){
            if(prior){
                if(op == nullptr){
                    op = nop;
                }else{
                    op_prior = nop;
                }
            }else{
                if(op != nullptr){
                    opdl = op(opdl, opdr);
                }
                op = nop;
            }
        }
    }
    return op == nullptr? opdl : op(opdl, opdr);
}


