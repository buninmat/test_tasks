#!/bin/bash

if [ $# -eq 0 ]
then
	echo Too little arguments
	exit 0
fi

ls "$1"

ifs="$IFS"
IFS=$'\n'
files=($(ls -S -w 1 "$1"))
IFS="$ifs"

for (( i=0; i<${#files[@]}; ++i ))
do
	name_old="${files[$i]}"
	name_new=$(printf "%03d" $((i + 1)))_"$name_old"
	mv "$1/$name_old" "$1/$name_new"
done

ls "$1"

