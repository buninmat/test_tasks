#include "comm.h"
#include <vector>

int main(int argc, char** argv){
	user_args ua = process_args(argc, argv);
	int fd = setup_tty(ua);
	size_t size = ua.baud * ua.timeout;
	std::vector<char> vec(size);
	std::string line;
	while(true){
		if(!vec.empty() && vec[0] != 0){
			std::cout << vec.data();
			for(char c : vec){
				if(c == '\n'){
					if(line == "term"){
						close(fd);
						return 0;
					}
					line.clear();
				}else if(c != 0) {
					line = line + c;
				}
			}
			vec.clear();
			vec.resize(size, 0);
		}else{
			std::cout << "listening to " << ua.dev << std::endl;
		}
		if(read(fd, vec.data(), vec.size()) == -1){
			close(fd);
			err("read");
		}
	}
	close(fd);
	return 0;
}

