#include <iostream>
#include <string>
#include <cstring>
#include <vector>

#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>

void err(std::string msg){
	std::cerr << msg << "\ncode "<< errno << ": " << strerror(errno) << std::endl;
	exit(errno);
}

std::vector<int> default_bauds = {B50,  B75,  B110,  B134,  B150,  B200,
	B300, B600, B1200, B1800, B2400, B4800, B9600, 
	B19200, B38400, B57600, B115200, B230400, B460800};

std::vector<int> default_bauds_vals = {50,  75,  110,  134,  150,  200,
	300, 600, 1200, 1800, 2400, 4800, 9600, 
	19200, 38400, 57600, 115200, 230400, 460800};

void write_bauds(){
	for(auto b : default_bauds_vals){
		std::cout << b << " ";
	}	
	std::cout << std::endl;
}

int default_baud(int val){
	for(size_t i = 0; i < default_bauds.size(); ++i){
		if(val == default_bauds_vals[i]){
			return default_bauds[i];
		}
	}
	return -1;
}

struct user_args{
	std::string dev;
	bool custom_boud;
	int baud;
	int timeout;
};

user_args process_args(int argc, char** argv){
	user_args ua{"", false, B19200, 5};
	enum exp {none, device, baud, timeout};
	exp next = exp::none;
       	for(int i = 1; i < argc; ++i){
		std::string arg = argv[i];
		if(arg == "-d"){
			next = exp::device;
			continue;
		}
		if(arg == "-c"){
			ua.custom_boud = true;
			next = exp::none;
			continue;
		}
		if(arg == "-b"){
			next = exp::baud;
			continue;
		}
		if(arg == "-t"){
			next = exp::timeout;
			continue;
		}
		if(next == exp::device){
			ua.dev = argv[i];
		}
		if(next == exp::baud){
			ua.baud = atoi(argv[i]);
			int def_baud = default_baud(ua.baud);
			if(def_baud == -1 && !ua.custom_boud){
				std::cout << "use -c or one of" << std::endl;
				write_bauds();
				exit(0);
			}
		}
		if(next == exp::timeout){
			ua.timeout = atoi(argv[i]);
		}
	}
	if(ua.dev == ""){
		std::cout << "no device specified" << std::endl;
		exit(0);
	}
	return ua;
}

int setup_tty(user_args ua){
	int fd = open(ua.dev.c_str(), O_RDWR | O_NOCTTY);
	if(fd < 0){
		err("Error while opening " + ua.dev);
	}
	struct termios tty;
	if(tcgetattr(fd, &tty) != 0){
		close(fd);
		err("tcgetattr");
	}

	tty.c_cc[VTIME] = ua.timeout;
	tty.c_cc[VMIN] = 1;

	tty.c_cflag &= ~PARENB; // no parity

	tty.c_cflag &= ~CSTOPB; // one stop bit

	tty.c_cflag &= ~CSIZE; // 8 bits per byte
	tty.c_cflag |= CS8;

	tty.c_cflag &= ~CRTSCTS; // disable RTS/CTS hardware flow control

	tty.c_cflag |= CREAD | CLOCAL; // disable DCD and hangup, enable read

	tty.c_lflag &= ~ICANON; // disable line-buffered (canonical) input

	tty.c_lflag &= ~(ECHO | ECHOE | ECHONL); // disable echoing

	tty.c_lflag &= ~ISIG; // not interpret INTR, QUIT, SUSP

	tty.c_iflag &= ~(IXON | IXOFF | IXANY); // software flow control

	tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); //input bytes processing

	tty.c_oflag &= ~OPOST; // output bytes processing
	tty.c_oflag &= ~ONLCR; // NL to CR

	if(ua.custom_boud){
		cfsetispeed(&tty, ua.baud);
		cfsetospeed(&tty, ua.baud);
	}else{
		cfsetspeed(&tty, ua.baud);
	}
	if(tcsetattr(fd, TCSANOW, &tty) != 0){
		close(fd);
		err("tcsetattr");
	}
	return fd;
}

