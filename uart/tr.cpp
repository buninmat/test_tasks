#include "comm.h"

int main(int argc, char** argv){
	int fd = setup_tty(process_args(argc, argv));
	std::string inp;
	do{
		std::cin >> inp;
		std::string msg = inp + "\n";
		if(write(fd, msg.c_str(), msg.size()) == -1){
			close(fd);
			err("write");
		}

	}while(inp != "term");
	close(fd);
	return 0;
}

