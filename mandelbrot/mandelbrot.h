#ifndef MANDELBROT_H
#define MANDELBROT_H
#include <QImage>
#include <vector>
#include <QDebug>
#include <QPromise>

#include "palette.h"
#include <omp.h>

class Mandelbrot
{
    std::vector<int> colormap;
    std::unique_ptr<QImage> image;
    int width;
    int height;

    struct complex_t{
        double a, j;
        complex_t(double a, double j): a(a), j(j){}
        complex_t operator +(complex_t other){
            return complex_t(a + other.a, j + other.j);
        }
        complex_t operator -(complex_t other){
            return complex_t(a - other.a, j - other.j);
        }
        complex_t operator *(double s){
            return complex_t(a * s, j * s);
        }
        complex_t square(){
            return complex_t(a * a - j * j, 2 * a * j);
        }
        double abs(){
            return sqrt(a * a + j * j);
        }
    };

    int cmapGet(int i, int j){
        return colormap[i * width + j];
    }

    void cmapSet(int i, int j, int value){
        colormap[i * width + j] = value;
    }

public:

    float scaleX = 3.5, scaleY = 2.;
    float offsetX = -2.5, offsetY = -1.;

    Mandelbrot(int width, int height)
        : width(width), height(height){
        colormap.resize(width * height);
        image = std::make_unique<QImage>(width, height, QImage::Format::Format_RGB32);
    }

    void compute(QPromise<void> &promise, int paletteSize, int maxSeqLen = 1000, double threshold = 2);

    void draw(QPromise<void> &promise, Palette &plt);

    QImage &getImage(){return *image.get();}

    complex_t toFractalCoord(int x, int y);
};

#endif // MANDELBROT_H
