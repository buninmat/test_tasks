#ifndef MOUSELABEL_H
#define MOUSELABEL_H
#include <QLabel>
#include <QMouseEvent>

class MouseLabel : public QLabel
{
    Q_OBJECT

public:
    MouseLabel():QLabel(){}
    virtual ~MouseLabel(){}

signals:
    void mouseMoved(int x, int y);
    void mouseReleased();
    void mouseScrolled(int offset, int x, int y);
    void mouseDoubleClicked(int x, int y);

protected:
    void mouseMoveEvent(QMouseEvent *ev)override{
        QPoint point = ev->pos();
        mouseMoved(point.x(), point.y());
    }
    void mouseReleaseEvent(QMouseEvent *) override{
        mouseReleased();
    }
    void wheelEvent(QWheelEvent *ev) override{
        QPointF pos = ev->position();
        mouseScrolled(ev->angleDelta().y(), pos.x(), pos.y());
    }
    void mouseDoubleClickEvent(QMouseEvent *ev) override{
        QPoint point = ev->pos();
        mouseDoubleClicked(point.x(), point.y());
    }
};

#endif // MOUSELABEL_H
