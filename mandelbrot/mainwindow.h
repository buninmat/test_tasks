#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFuture>
#include <QtConcurrent>
#include <QDesktopServices>
#include <QFileDialog>
#include <QMessageBox>
#include <QLayout>
#include <QPromise>
#include "mouselabel.h"

#include "mandelbrot.h"
#include "palette.h"
#include "utils.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; class MouseLabel; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void updateMandelbrot();
    void onMouseMove(int x, int y);
    void onMouseRelease();
    void onMouseScroll(int offset, int x, int y);

    void browsePalette();

    void browseSavePath();

    void save();

    void redrawMandelbrot();

    void updateComputeSettings();

private:
    Ui::MainWindow *ui;
    MouseLabel* imageLabel;

    std::unique_ptr<Mandelbrot> mandelbrot;

    Palette palette;
    int iterations = 50;
    double threshold = 100.;
    int width = 1000;
    int height = 800;

    QFuture<void> future;
    QFutureWatcher<void> watcher;
    QString savePath;

    void showMandelbrot();

    void computeAsync();

    void cancelCompute();

    QString fixExtention(QString filter);

    void updatePalette(QString path);

    QString imageFilter(){
        return "Imges (" + utils::supportedImageExtentions("*.").join(" ") + ")";
    }

    QString paletteFilter(){
        return "*.plt";
    }
};
#endif // MAINWINDOW_H
