#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QLayout>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    imageLabel = new MouseLabel;
    ui->centralwidget->layout()->addWidget(imageLabel);

    connect(ui->lineEdit_height, &QLineEdit::editingFinished, this, &MainWindow::updateMandelbrot);
    connect(ui->lineEdit_width, &QLineEdit::editingFinished, this, &MainWindow::updateMandelbrot);
    connect(ui->lineEdit_iter, &QLineEdit::editingFinished, this, &MainWindow::updateComputeSettings);
    connect(ui->lineEdit_thr, &QLineEdit::editingFinished, this, &MainWindow::updateComputeSettings);

    connect(imageLabel, &MouseLabel::mouseMoved, this, &MainWindow::onMouseMove);
    connect(imageLabel, &MouseLabel::mouseReleased, this, &MainWindow::onMouseRelease);
    connect(imageLabel, &MouseLabel::mouseScrolled, this, &MainWindow::onMouseScroll);

    connect(ui->pushButton_save, &QPushButton::pressed, this, &MainWindow::save);

    connect(ui->pushButton_browseSave, &QPushButton::pressed, this, &MainWindow::browseSavePath);
    connect(ui->lineEdit_savePath, &QLineEdit::editingFinished, this, [&]()->void{savePath = ui->lineEdit_savePath->text();});

    connect(ui->pushButton_browsePalette, &QPushButton::pressed, this, &MainWindow::browsePalette);
    connect(ui->lineEdit_palette, &QLineEdit::editingFinished, this, [&]()->void{updatePalette(ui->lineEdit_palette->text());});

    ui->lineEdit_width->setText(QString::number(width));
    ui->lineEdit_height->setText(QString::number(height));
    ui->lineEdit_iter->setText(QString::number(iterations));
    ui->lineEdit_thr->setText(QString::number(threshold));
    updateMandelbrot();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showMandelbrot(){
    qDebug() << "show " << thread()->currentThreadId();
    imageLabel->setPixmap(QPixmap::fromImage(mandelbrot->getImage()).scaled(1000, 800));
}

void MainWindow::updateMandelbrot(){
    bool heightValid, widthValid;
    int width = ui->lineEdit_width->text().toInt(&heightValid);
    int height = ui->lineEdit_height->text().toInt(&widthValid);
    if(!heightValid || !widthValid){
        return;
    }
    cancelCompute();
    mandelbrot = std::make_unique<Mandelbrot>(width, height);
    computeAsync();
}

void MainWindow::updateComputeSettings(){
    bool iterValid, thrValid;
    iterations = ui->lineEdit_iter->text().toInt(&iterValid);
    threshold = ui->lineEdit_thr->text().toInt(&thrValid);
    if(!iterValid || !thrValid){
        return;
    }
    computeAsync();
}

void MainWindow::computeAsync(){
    cancelCompute();
    future = QtConcurrent::run([&](QPromise<void> &p)->void{mandelbrot->compute(p, palette.size(), iterations, threshold);});

    qDebug() << "main " << thread()->currentThreadId();
    watcher.setFuture(future);

    disconnect(&watcher, &QFutureWatcher<void>::finished, this, &MainWindow::showMandelbrot);
    connect(&watcher, &QFutureWatcher<void>::finished, this, &MainWindow::redrawMandelbrot);
}

bool released = true;
int offsetX = 0, offsetY = 0;

void MainWindow::onMouseMove(int x, int y){
    static int x0, y0;
    if(released){
        x0 = x; y0 = y;
    }
    released= false;
    offsetX = x - x0;
    offsetY = y - y0;
}

void MainWindow::onMouseRelease(){
    released = true;
    cancelCompute();
    mandelbrot->offsetX -= offsetX / (float)imageLabel->width() * mandelbrot->scaleX;
    mandelbrot->offsetY -= offsetY / (float)imageLabel->height() * mandelbrot->scaleY;
    offsetX = 0;
    offsetY = 0;
    computeAsync();
}

void MainWindow::onMouseScroll(int offset, int x, int y){
    cancelCompute();
    float ds = (offset < 0? -1 : 1) * 0.1;
    auto p = mandelbrot->toFractalCoord(x,y);
    mandelbrot->scaleX *= 1.f + ds;
    mandelbrot->scaleY *= 1.f + ds;
    auto pd = mandelbrot->toFractalCoord(x,y) - p;
    mandelbrot->offsetX -= pd.a;
    mandelbrot->offsetY -= pd.j;
    computeAsync();
}

void MainWindow::cancelCompute(){
    if(watcher.isRunning()){
        watcher.cancel();
        watcher.waitForFinished();
    }
}

void MainWindow::updatePalette(QString path){
    auto newPalette = Palette(path);
    if(newPalette.size() != 0){
        palette = newPalette;
    }
    redrawMandelbrot();
}

void MainWindow::redrawMandelbrot(){
    cancelCompute();
    future = QtConcurrent::run([&](QPromise<void> &p)->void{mandelbrot->draw(p, palette);});
    watcher.setFuture(future);
    disconnect(&watcher, &QFutureWatcher<void>::finished, this, &MainWindow::redrawMandelbrot);
    connect(&watcher, &QFutureWatcher<void>::finished, this, &MainWindow::showMandelbrot);
}

void MainWindow::save(){
    mandelbrot->getImage().save(savePath);
}

void MainWindow::browsePalette(){
    QString path = QFileDialog::getOpenFileName(this, "Open...", QDir::toNativeSeparators(QApplication::applicationDirPath()), paletteFilter());
    updatePalette(path);
    ui->lineEdit_palette->setText(path);
}

void MainWindow::browseSavePath(){
    savePath = QFileDialog::getSaveFileName(this, "Save as...(Export)", QDir::toNativeSeparators(QApplication::applicationDirPath()), imageFilter());
    QString ext = utils::getExtention(savePath);
    if(!utils::supportedImageExtentions().contains(ext)){
        QMessageBox mb;
        mb.setText("I will use the default extension instead of *." + ext);
        mb.exec();
        savePath = utils::getDirectoryName(savePath) + utils::getFileName(savePath).split(".")[0] + ".png";
    }
    ui->lineEdit_savePath->setText(savePath);
}


