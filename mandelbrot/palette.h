#ifndef PALETTE_H
#define PALETTE_H
#include <QColor>
#include <QFile>
#include <QTextStream>
#include <QRegularExpression>

class Palette
{
    std::vector<QColor> colors;

    QString path;

public:
    Palette();

    Palette(QString path);

    QColor &getColor(int ind) {
        return colors[ind];
    }

    size_t size(){
        return colors.size();
    }

    QString getPath(){
        return path;
    }
};

#endif // PALETTE_H
