#include "mandelbrot.h"
#include<QThread>

void Mandelbrot::compute(QPromise<void> &promise, int paletteSize, int maxSeqLen, double threshold){
    qDebug() << "compute " << QThread::currentThreadId();
    static bool envSet = false;
    if(!envSet){
        envSet = true;
        putenv(const_cast<char*>("OMP_CANCELLATION=true"));
    }
#pragma omp parallel for schedule(static)
    for(int i = 0; i < height; ++i){
        for(int j = 0; j < width; ++j){
            if(promise.isCanceled()){
#pragma omp cancel for
            }
            complex_t c = toFractalCoord(j, i);
            complex_t z(0, 0);
            int len = 0;
            while(z.abs() <= threshold && len < maxSeqLen){
                z = z.square() + c;
                len++;
            }
            //len = maxSeqLen - len;
            cmapSet(i, j, int(len / (double) maxSeqLen * (paletteSize - 1)));
        }
    }
}

void Mandelbrot::draw(QPromise<void> &promise, Palette &plt){
    qDebug() << "draw " << QThread::currentThreadId();

    for(int i = 0; i < height; ++i){
        for(int j = 0; j < width; ++j){
            //qDebug() << plt.getColor(cmapGet(i, j));
            if(promise.isCanceled()){
                break;
            }
            image->setPixelColor(j, i, plt.getColor(cmapGet(i, j)));
        }
    }
}

Mandelbrot::complex_t Mandelbrot::toFractalCoord(int x, int y){
    return complex_t (x / (double)width * scaleX + offsetX, y / (double)height * scaleY + offsetY);
}
