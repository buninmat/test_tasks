#include "palette.h"


Palette::Palette(QString path):path(path){
    QFile file(path);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        return;
    }
    QTextStream fin(&file);
    while(!fin.atEnd()){
        QString line = fin.readLine();
        QRegularExpression re("[0-9]+");
        QRegularExpressionMatchIterator matchIt = re.globalMatch(line);
        QString r = matchIt.next().captured(0);
        QString g = matchIt.next().captured(0);
        QString b = matchIt.next().captured(0);
        QColor col(r.toInt(),g.toInt(),b.toInt());
        colors.push_back(col);
    }
}

Palette::Palette(){
    for(int i = 0; i < 256; ++i){
        colors.push_back(QColor(i,i,i));
    }
}
