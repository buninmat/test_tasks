#ifndef UTILS_H
#define UTILS_H
#include <QString>
#include <QStringList>
#include <QDir>
#include <QImageReader>

namespace utils {
    QString getFileName(QString path);
    QString getDirectoryName(QString path);
    QStringList supportedImageExtentions(QString prefix = "");
    QString getExtention(QString path);
}

#endif // UTILS_H
